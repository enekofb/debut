#Debut exercise

## Design/Implementations notes

- Bounded context are business cohesion and transactional boundaries. Two have been identified

1. Events: Event as aggregate root with Invite as Entity.

2. Users: User as aggregate root with Invite as Entity.

It results in that we dont have strong consistency between event <-> invite <-> user. This is fundamentally 
DDD principle that mandates to have eventual consistent updates through Domain Events.

Domain Events hasnt been modeled due to the scope of the excersise but they should be flowing 
between bounded context.

About communication between context, we haven deployed a messing system (simplification) so we need to rely 
in Anemic Domain style design in order to have somehow transactionality. 

About Testing development. It has been implemented by doing BDD/TDD cycles it means that both 
BDD/TDD have been used for acceptance and desing. It also means that Unit testing is not complete as a full regression 
suite and more tests to exercise the contracts should be done to have the whole suite. 

- Openweathermap access apikey has been encrypted. Ask me for the password if I havent given to you already.

- TODOs has been intentionally added for explaining what else needs to be done

- We havent deployed full DTO layer for the different messages but a subset of them in order to put that in action 
but not the intention to over complicate the solution. Same applies for Mappers, Gateways, Adapters ... 
