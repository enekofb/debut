package careers.debut.users.service;

import careers.debut.events.models.Invite;
import careers.debut.events.models.InvitationStatus;
import careers.debut.users.models.User;
import careers.debut.users.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Component
@Slf4j
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public User createUser(@NotNull  User user) {
        log.info("Create user {}", user);
        User createdUser = userRepository.save(user);
        log.info("Created user {}", createdUser);
        return createdUser;
    }

    @Cacheable("getAllInviteeByEmail")
    public Collection<User> findAllByEmail(@NotNull  Collection<String> inviteeEmails) {
        log.info("find users by email {}", inviteeEmails);
        List<User> foundUsersByEmail = inviteeEmails.stream()
                .map(userRepository::findByEmail)
                .collect(Collectors.toList());
        log.info("found {} users", foundUsersByEmail.size());
        return foundUsersByEmail;
    }

    @Cacheable("getInviteForUser")
    public Collection<Invite> getInviteForUserByUserId(@NotNull  String userId) {
        log.info("find invites for users {}", userId);
        Collection<Invite> eventInvitations = userRepository.findOne(userId)
                .getEventInvitations();
        log.info("found {} invites", eventInvitations.size());
        return eventInvitations;
    }

    public Collection<Invite> getInviteForUserByUserIdAndStatus(@NotNull  String userId
            , @Valid  InvitationStatus invitationStatus) {
        log.info("find invites for users {} with status {}", userId,invitationStatus.name());

        //TODO: move to spring data named query
        List<Invite> invitesFound = getInviteForUserByUserId(userId).stream()
                .filter(invitation ->
                        invitation.getStatus().equals(invitationStatus))
                .collect(Collectors.toList());
        log.info("foung {} invites with status {} ", invitesFound.size(),invitationStatus.name());
        return invitesFound;
    }

    public Invite updateInvite(@NotNull String userId, @NotNull String eventId, @Valid Invite updateInvite) {
        log.info("update invite {} {}",userId,eventId);
        User user = this.userRepository.findOne(userId);
        Invite invitation = user.getEventInvitations().stream().filter(eventInvite -> eventInvite
                .getEvent()
                .equals(eventId))
                .findFirst()
                .orElseThrow(RuntimeException::new);
        invitation.setStatus(updateInvite.getStatus());
        this.userRepository.save(user);
        log.info("updated invite {} {} status is {}",userId,eventId,invitation.getStatus().name());
        return invitation;
    }
}
