package careers.debut.users.models;

import careers.debut.events.models.Invite;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * Users can be invited to events
 *
 */
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class User {
    @Id
    @GeneratedValue
    private String id;

    private String email;

    @ElementCollection
    @Builder.Default
    private Set<Invite> eventInvitations = new HashSet<>();

    public boolean addInvitee(Invite invitation) {
        return this.eventInvitations.add(invitation);
    }
}
