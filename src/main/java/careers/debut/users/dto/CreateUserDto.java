package careers.debut.users.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CreateUserDto {

    private String email;

}
