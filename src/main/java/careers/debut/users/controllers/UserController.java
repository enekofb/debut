
package careers.debut.users.controllers;

import careers.debut.users.dto.CreateUserDto;
import careers.debut.users.dto.UserDto;
import careers.debut.events.models.InvitationStatus;
import careers.debut.events.models.Invite;
import careers.debut.users.models.User;
import careers.debut.users.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/users")
@Slf4j
public class UserController {

    @Autowired
    private ModelMapper mapper;

    @Autowired
    private UserService userService;

    @PostMapping
    public UserDto createUser(@Valid @RequestBody CreateUserDto userDto)  {
        log.info("Create user request for {}",userDto.toString());
        User user = mapper.map(userDto,User.class);
        User createdUser = userService.createUser(user);
        return mapper.map(createdUser,UserDto.class);
    }


    @GetMapping("/{userId}/invites")
    public Collection<Invite> getUserInvitesByUserIdAndStatus(@PathVariable("userId") String userId,
                                                              @NotNull @RequestParam Map<String, String> filters) {
        log.info("Get invite list for user {}", userId);
        String inviteStatusString = Optional.ofNullable(filters.get("status")).orElse(null);

        Collection<Invite> inviteDto = inviteStatusString == null ?
                userService.getInviteForUserByUserId(userId) :
                userService.getInviteForUserByUserIdAndStatus(userId,
                        InvitationStatus.valueOf(inviteStatusString));
        return inviteDto;
    }

    @PutMapping("/{userId}/invites/{eventId}")
    public Invite updateInvite(@PathVariable("userId") String userId,
                                      @PathVariable("eventId") String eventId,
                                      @Valid @RequestBody Invite invitationDto) {
        log.info("Udpate invite for user {} and event {}", userId,eventId );
        return userService.updateInvite(userId,eventId,invitationDto);
    }
}
