package careers.debut.events.service;

import careers.debut.events.dto.CreateEventDto;
import careers.debut.events.dto.EventDto;
import careers.debut.events.dto.EventDtoWithForecast;
import careers.debut.events.models.Event;
import careers.debut.events.models.InvitationStatus;
import careers.debut.events.models.Invite;
import careers.debut.users.models.User;
import careers.debut.events.repository.EventRepository;
import careers.debut.users.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.lang.reflect.Type;
import java.util.*;
import java.util.stream.Collectors;

@Component
@Slf4j
public class EventService {

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private ForecastService forecastService;

    //TODO: refactor me to a proper mapper. too much mapping logic in the service
    @Autowired
    private ModelMapper mapper;

    @Transactional
    public EventDto createEvent(CreateEventDto createEventDto) {
        log.info("Create event for {}", createEventDto);
        Event event = mapCreateEventDtoToEvent(createEventDto);
        Event createdEvent = eventRepository.save(event);
        log.info("Created event {}", createdEvent);
        return mapEventToEventDto(createdEvent);
    }

    //TODO: move me
    protected Event mapCreateEventDtoToEvent(CreateEventDto createEventDto) {
        Event event = mapper.map(createEventDto, Event.class);
        Collection<User> usersToInvite = userService.
                findAllByEmail(createEventDto.getInvitees());
        event.setInvitees(createInvitees(event, usersToInvite));
        return event;
    }

    //TODO: move me
    protected EventDto mapEventToEventDto(Event event) {
        return EventDto.builder()
                .id(event.getId())
                .name(event.getName())
                .capacity(event.getCapacity())
                .location(event.getLocation())
                .visibility(event.getVisibility().name())
                .invitees(event.getInvitees())
                .build();
    }

    private Set<Invite> createInvitees(Event event, Collection<User> userToInvite) {
        return userToInvite.stream()
                .map(invitee -> createInvitee(event, invitee))
                .collect(Collectors.toSet());
    }

    private Invite createInvitee(Event event, User invitee) {
        return new Invite(event.getId(), invitee.getId(), InvitationStatus.NOT_ANSWERED);
    }

    @Cacheable("getEventsByLocation")
    public Collection<EventDto> findByLocation(String location) {
        log.info("Get events for location {}", location);
        Collection<Event> foundsEventByLocation = eventRepository.findByLocation(location);
        log.info("Found events {} for location {}", foundsEventByLocation.size(), location);
        return mapToCollection(foundsEventByLocation);
    }

    protected Collection<EventDto> mapToCollection(Collection<Event> events) {
        Type listType = new TypeToken<Collection<EventDto>>() {
        }.getType();
        return mapper.map(events, listType);
    }

    public Collection<Invite> getInviteeForEventByEventIdAndStatus(String eventId, @Valid InvitationStatus invitationStatus) {
        log.info("Get invitee for event {} by status {}", eventId, invitationStatus);

        //TODO: move to spring data named query
        List<Invite> inviteesFound = getInviteeForEventByEventId(eventId).stream()
                .filter(invitation -> invitation.getStatus().equals(invitationStatus))
                .collect(Collectors.toList());
        log.info("Invitees found {}", inviteesFound.size());
        return inviteesFound;
    }

    @Cacheable("getInviteeForEventByEventId")
    public Collection<Invite> getInviteeForEventByEventId(String eventId) {
        log.info("Get invitee for event {} by status {}", eventId);
        Collection<Invite> invitees = eventRepository.findOne(eventId).getInvitees();
        log.info("Invitees found {}", invitees.size());
        return invitees;
    }

    public EventDtoWithForecast getEventForecastById(String eventId) {
        log.info("Get forecast for event {}", eventId);

        Event event = eventRepository.findOne(eventId);
        if (!event.isForecastPredicatble()) {
            throw new RuntimeException("Forecast not predictable");
        }

        String forecastForLocation = forecastService.getForecastForLocation(event.getLocation());

        log.info("Forecast found", eventId);
        return mapEventToEventDtoWithLocation(event, forecastForLocation);
    }

    private EventDtoWithForecast mapEventToEventDtoWithLocation(Event event, Object forecastForLocation) {
        return new EventDtoWithForecast(event.getId(), forecastForLocation.toString());
    }

    @Cacheable("findAllEvents")
    public Collection<EventDto> findAllWithFilters(Map<String, String> filters) {
        log.info("Find events with filter");
        int page = Integer.valueOf(Optional.ofNullable(filters.get("page")).orElse("0"));
        int size = Integer.valueOf(Optional.ofNullable(filters.get("size")).orElse("20"));
        Direction sortDirection = Direction.valueOf(Optional.ofNullable(filters.get("sortDirection"))
                .orElse("ASC"));
        String sortBy = Optional.ofNullable(filters.get("sortBy")).orElse("id");

        PageRequest sortByDateAndWithLimit = new PageRequest(page, size, sortDirection, sortBy);

        List<Event> foundsEventByLocation = eventRepository.findAll(sortByDateAndWithLimit)
                .getContent();
        Collection<EventDto> eventDtos = mapToCollection(foundsEventByLocation);
        log.info("Found {} events with filter",eventDtos.size());
        return eventDtos;

    }
}
