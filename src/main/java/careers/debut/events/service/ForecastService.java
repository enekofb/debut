package careers.debut.events.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Component
public class ForecastService {

    //TODO: refactor me to ForecastConfig
    @Value("${openweathermap.apikey}")
    private String apikey;

    @Value("${openweathermap.endpoint}")
    private String endpoint;

    //TODO: refactor me to ForecastGateway
    @Autowired
    private RestTemplate restTemplate;

    public ForecastService(){

    }

    protected ForecastService(String apiKey, String endpoint,RestTemplate restTemplate) {
        this.apikey = apiKey;
        this.endpoint = endpoint;
        this.restTemplate = restTemplate;
    }

    public String getForecastForLocation(String location) {
        String getForecastForLocationUrl = UriComponentsBuilder.fromHttpUrl(endpoint)
                .queryParam("q", location)
                .queryParam("APPID",apikey)
                .build()
                .toString();
        return restTemplate.getForObject(getForecastForLocationUrl, String.class);
    }
}
