package careers.debut.events.models;

public enum InvitationStatus {
    NOT_ANSWERED,ACCEPTED, REJECTED
}
