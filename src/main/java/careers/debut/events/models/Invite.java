package careers.debut.events.models;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Embeddable
@AllArgsConstructor
@NoArgsConstructor
public class Invite implements Serializable {

    private String event;

    private String user;

    private InvitationStatus status;


}
