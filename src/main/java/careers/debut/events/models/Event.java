package careers.debut.events.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import static java.time.temporal.ChronoUnit.DAYS;

/**
 *
 */

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Event {


    private static final long MAX_PREDICTABLE_DAYS = 5;

    public enum Visibility {PUBLIC,PRIVATE;}
    @Id
    @GeneratedValue
    private String id;

    private String name;

    private int capacity;

    @Builder.Default private Visibility visibility = Visibility.PUBLIC;

    private String location;

    private LocalDate date;

    @ElementCollection
    @Builder.Default private Set<Invite> invitees = new HashSet<>();

    public boolean addInvitee(Invite invitation) {
        return this.invitees.add(invitation);
    }

    public boolean isForecastPredicatble() {
        return isWithinFiveDays();

    }

    private boolean isWithinFiveDays() {

        long daysForTheEvent = DAYS.between(LocalDate.now(),getDate());

        return daysForTheEvent >= 0 && daysForTheEvent <= MAX_PREDICTABLE_DAYS;
    }

}
