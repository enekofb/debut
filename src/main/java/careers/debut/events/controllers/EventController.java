
package careers.debut.events.controllers;

import careers.debut.events.dto.CreateEventDto;
import careers.debut.events.dto.EventDto;
import careers.debut.events.dto.EventDtoWithForecast;
import careers.debut.events.models.InvitationStatus;
import careers.debut.events.models.Invite;
import careers.debut.events.service.EventService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.Map;

@RestController
@RequestMapping("/events")
@Slf4j
public class EventController {

    public static final String FILTER_BY_LOCATION = "location";


    @Autowired
    private EventService eventService;

    //TODO: use a property exception with event dto details
    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<EventDtoWithForecast> handle(RuntimeException ex) {
        return new ResponseEntity<EventDtoWithForecast>(new EventDtoWithForecast(null,null),
                HttpStatus.EXPECTATION_FAILED);

    }

    @PostMapping
    public EventDto createEvent(@Valid @RequestBody CreateEventDto createEventDto) {
        log.info("Create event request for {}", createEventDto.toString());
        return eventService.createEvent(createEventDto);
    }

    @GetMapping
    public Collection<EventDto> findEventsWithFilters(@NotNull @RequestParam Map<String, String> filters) {
        log.info("Find events with filters {}", filters);
        if (filters.containsKey(FILTER_BY_LOCATION)) {
            return eventService.findByLocation(filters.get(FILTER_BY_LOCATION));
        }
        return eventService.findAllWithFilters(filters);
    }

    @GetMapping("/{eventId}/invitees")
    public Collection<Invite> getEventInviteesByEventIdAndStatus(@PathVariable("eventId") String eventId,
                                                                 @NotNull @RequestParam Map<String, String> filters) {
        log.info("Get invitee list for an event", eventId);
        String invitationStatusString = filters.get("status");

        Collection<Invite> invitationDtos = invitationStatusString == null ?
                eventService.getInviteeForEventByEventId(eventId) :
                eventService.getInviteeForEventByEventIdAndStatus(eventId,
                        InvitationStatus.valueOf(invitationStatusString));
        return invitationDtos;
    }

    @GetMapping("/{eventId}/forecast")
    public EventDtoWithForecast getEventForecastById(@PathVariable("eventId") String eventId) {
        log.info("Get forecast for event {} ", eventId);
        return eventService.getEventForecastById(eventId);

    }



}
