package careers.debut.events.repository;

import careers.debut.events.models.Event;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Collection;

public interface EventRepository extends PagingAndSortingRepository<Event, String> {

    Collection<Event> findByLocation(String location);

    Event findByName(String eventName);
}
