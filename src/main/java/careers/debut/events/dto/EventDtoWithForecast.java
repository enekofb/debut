package careers.debut.events.dto;

import lombok.Value;

@Value
public class EventDtoWithForecast {

    private String id;

    private String forecastDetails;

}
