package careers.debut.events.dto;

import careers.debut.events.models.Invite;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Collection;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class EventDto {

    private String id;

    private String name;

    private Collection<Invite> invitees;

    private int capacity;

    private String visibility;

    private String location;

}
