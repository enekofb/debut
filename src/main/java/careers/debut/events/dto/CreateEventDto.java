package careers.debut.events.dto;

import lombok.Builder;
import lombok.Data;

import java.util.Collection;

@Data
@Builder
public class CreateEventDto {

    private Collection<String> invitees;

    private int capacity;

    private String visibility;

    private String location;

    private String name;

}
