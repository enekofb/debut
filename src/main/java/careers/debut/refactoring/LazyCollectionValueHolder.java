package careers.debut.refactoring;

import lombok.experimental.Delegate;

import java.util.Collection;
import java.util.Optional;

/**
 *
 * Collection holder for heavy resource-consuming objects.
 * This holder should only fetch the information once only after the client calls it,
 * and in the following calls it must use the cached version.
 *
 * ie:
 *  <pre>
 * {@code
 *
 *
 *         Collection<String> holder = new LazyCollectionValueHolder<String>() {
                protected Collection<String> doGet() {
                    System.out.println("Calling delegate");
                    return Arrays.asList("A", "B", "C");
                }
            };

 *          assert holder.size() == 3; // Here delegate is invoked for the first time
 *
 * }
 * </pre>
 *
 */
public abstract class LazyCollectionValueHolder<E> implements Collection<E> {

    private Collection<E> delegate;

    @Delegate
    private Collection<E> getDelegate(){
        return Optional.
                ofNullable(delegate)
                .orElseGet(() -> delegate = doGet());
    }

    protected abstract Collection<E> doGet();

}

