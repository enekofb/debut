Feature: Create Event
  As an event organizer for company Events LTD
  I want to create new events in the platform
  So I can promote a service that one of our customers provides

  Background:
    Given the following users exist in the system
      | userEmail       |
      | john@gmail.com  |
      | david@gmail.com |

  Scenario Outline: I can create an event
    Given I want to create an event with name "<name>"
    And the event has max 100 seats
    And the visibility is "<visibility>"
    And the location is "London"
    And a list of invitees for the event
      | john@gmail.com | david@gmail.com |
    When I create the event
    Then the event has been created
    Examples:
      | name          | visibility |
      | public_event  | PUBLIC     |
      | private_event | PRIVATE    |