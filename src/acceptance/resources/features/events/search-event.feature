Feature: Search Event
  As a user of Events platfom
  I want to search events in the platform
  So I can promote a service that one of our customers provides

  Background:
    Given the following events exist in the system
      | eventName        | date       | location   |
      | london_event     | 2018-02-21 | London     |
      | manchester_event | 2018-09-23 | Manchester |
      | london_event2    | 2018-09-23 | London     |

  Scenario Outline: I can search event by location
    When I search al the events in "<location>"
    Then I receive a list of event with "<eventsInLocation>"
    Examples:
      | location   | eventsInLocation |
      | London     | 2                |
      | Manchester | 1                |
      | Leeds      | 0                |