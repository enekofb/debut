Feature: Get Event Invitee
  As an admin of Events platfom
  I want to get an event invitee list
  So I can contact invitees for different reasons

  Background:
    Given the following events exist in the system
      | eventName        | date       | location   |
      | london_event     | 2018-02-21 | London     |
      | manchester_event | 2018-09-23 | Manchester |
    And the following users exist in the system
      | userEmail           |
      | john@gmail.com  |
      | david@gmail.com |
    And the following invitations exists in the system
      | eventName        | userEmail       | invitationStatus |
      | london_event     | john@gmail.com  | ACCEPTED         |
      | manchester_event | david@gmail.com | REJECTED         |
      | manchester_event | john@gmail.com  | ACCEPTED         |

  Scenario Outline: I can get a list of invitees for an event
    When I get the invitees list for event "<eventName>"
    Then I receive a invitee list of event with "<inviteesSize>"
    Examples:
      | eventName        | inviteesSize |
      | london_event     | 1            |
      | manchester_event | 2            |

#  Scenario Outline: I can get a list of accepted invitations for an event
#    When I get list of accepted invitations for event "<eventName>"
#    Then I receive a invitee list of event with "<inviteesSize>"
#    Examples:
#      | eventName        | inviteesSize |
#      | london_event     | 1            |
#      | manchester_event | 1            |
#
#  Scenario Outline: I can get a list of rejected invitations for an event
#    When I get list of rejected invitations for event "<eventName>"
#    Then I receive a invitee list of event with "<inviteesSize>"
#    Examples:
#      | eventName        | inviteesSize |
#      | london_event     | 0            |
#      | manchester_event | 1            |
