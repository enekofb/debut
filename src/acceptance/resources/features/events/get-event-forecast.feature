Feature: Get Event forecast
  As a user of Events platform that goes to an event in within 5 days
  I want to get the forecast for the day of the event
  So I can plan my attendance to it

  Background:
    Given the following events exist in the system
      | eventName        | date  | location   |
      | london_event     | today | London     |
      | manchester_event | month | Manchester |

  Scenario: I can get forecast for an event within 5 days
    Given I am going to event "london_event"
    When I get its forecast
    Then I receive its forecast

  Scenario: I cannot get forecast for events to come in more than 5 days
    Given I am going to event "manchester_event"
    When I get its forecast
    Then I dont receive its forecast so its not predictable yet