Feature: Get upcoming Events
  As a user of Events platfoRm
  I want to get the upcoming events
  So I can plan my attendance to them

  Background:
    Given the following events exist in the system
      | eventName        | date       | location   |
      | london_event     | 2018-02-21 | London     |
      | london_event2    | 2018-02-22 | London     |
      | manchester_event | 2018-02-23 | Manchester |

  Scenario Outline: I can get upcoming events
    When I search the "<limit>" events to come
    Then I receive a list with following events "<eventsInLimit>"
    Examples:
      | limit | eventsInLimit                               |
      | 1     | london_event                                |
      | 2     | london_event,london_event2                  |
      | 3     | london_event,london_event2,manchester_event |