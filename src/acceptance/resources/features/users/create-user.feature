Feature: Create User
  As an admin of Events LTD
  I want to create new users in the platform
  So they can interact with the events of the platform

  Scenario: I can create a user with email
    When I create a user with email "john@gmail.com"
    Then the user has been created

