Feature: Get User Invitations
  As a User of Events platform
  I want to get the list of events I have been invited to
  So I can manage them

  Background:
    Given the following events exist in the system
      | eventName        | date       | location   |
      | london_event     | 2018-02-21 | London     |
      | manchester_event | 2018-09-23 | Manchester |
    And the following users exist in the system
      | userEmail           |
      | john@gmail.com  |
      | david@gmail.com |
    And the following invitations exists in the system
      | eventName        | userEmail       | invitationStatus |
      | london_event     | john@gmail.com  | ACCEPTED         |
      | manchester_event | david@gmail.com | REJECTED         |
      | manchester_event | john@gmail.com  | ACCEPTED         |

  Scenario Outline: I can get a list of invites for a given user
    When I get the invite list for user "<userEmail>"
    Then I receive an invite with "<invitesNumber>"
    Examples:
      | userEmail       | invitesNumber |
      | john@gmail.com  | 2             |
      | david@gmail.com | 1             |

  Scenario Outline: I can get a list of the events I have accepted
    When I get list of accepted invites for user "<userEmail>"
    Then I receive an invite with "<invitesNumber>"
    Examples:
      | userEmail       | invitesNumber |
      | john@gmail.com  | 2             |
      | david@gmail.com | 0             |
