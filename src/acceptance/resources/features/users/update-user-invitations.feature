Feature: Update User Invite
  As a User of Events platform
  I want to update an invite
  So I can decide whether to assist or not

  Background:
    Given the following events exist in the system
      | eventName        | date       | location   |
      | london_event     | 2018-02-21 | London     |
      | manchester_event | 2018-09-23 | Manchester |
    And the following users exist in the system
      | userEmail       |
      | john@gmail.com  |
      | david@gmail.com |
    And the following invitations exists in the system
      | eventName        | userEmail       | invitationStatus |
      | london_event     | john@gmail.com  | NOT_ANSWERED     |
      | manchester_event | david@gmail.com | NOT_ANSWERED     |
      | manchester_event | john@gmail.com  | NOT_ANSWERED     |

  Scenario Outline: I can udpate an event invite
    Given I get the invite for event "<eventName>"  for user "<userEmail>"
    When I update the invite status to "<invitationStatus>"
    Then the invite has been updated
    Examples:
      | eventName    | userEmail      | invitationStatus |
      | london_event | john@gmail.com | ACCEPTED         |
      | london_event | john@gmail.com | REJECTED         |

  Scenario: I can change an updated invite
    Given I get the invite for event "london_event"  for user "john@gmail.com"
    When I update the invite status to "ACCEPTED"
    Then the invite has been updated
    When I update the invite status to "REJECTED"
    Then the invite has been updated