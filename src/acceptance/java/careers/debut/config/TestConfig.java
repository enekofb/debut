package careers.debut.config;

import careers.debut.common.DebutWorld;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.filter.CommonsRequestLoggingFilter;

@Configuration
public class TestConfig {

    @Bean
    public TestRestTemplate testRestTemplate(TestRestTemplate testRestTemplate,
                                             SecurityProperties securityProperties) {
        return testRestTemplate.
                withBasicAuth(securityProperties.getUser().getName(),
                              securityProperties.getUser().getPassword());
    }

    @Bean
    public DebutWorld debutWorld(){
        return new DebutWorld();
    }

    @Bean
    public CommonsRequestLoggingFilter logFilter() {
        CommonsRequestLoggingFilter filter
                = new CommonsRequestLoggingFilter();
        filter.setIncludeQueryString(true);
        filter.setIncludePayload(true);
        filter.setMaxPayloadLength(10000);
        filter.setIncludeHeaders(false);
        filter.setAfterMessagePrefix("REQUEST DATA : ");
        return filter;
    }
}
