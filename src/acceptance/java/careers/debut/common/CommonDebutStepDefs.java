package careers.debut.common;

import careers.debut.ExerciseApplication;
import careers.debut.config.TestConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

@SpringBootTest(classes = {ExerciseApplication.class,TestConfig.class },
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration
@ActiveProfiles("test")
public class CommonDebutStepDefs {

    protected Logger LOGGER = LoggerFactory.getLogger(getClass());

    //TODO: should we move invitees to users?
    protected static final String CREATE_EVENT_URL = "http://localhost:{port}/events";
    protected static final String SORTED_BY_STRING = "?page={page}&size={size}&sortDirection={sortDirection}&sortBy={sortBy}";
    protected static final String GET_ALL_EVENTS_SORTED_BY = "http://localhost:{port}/events" + SORTED_BY_STRING ;
    protected static final String GET_EVENT_INVITEE_BY_ID = "http://localhost:{port}/events/{eventId}/invitees";
    protected static final String GET_EVENT_INVITEE_BY_STATUS = "http://localhost:{port}/events/{eventId}/invitees?status={status}";
    protected static final String GET_EVENT_FORECAST_BY_ID = "http://localhost:{port}/events/{eventId}/forecast";

    protected static final String GET_EVENTS_BY_CRITERIA_URL = "http://localhost:{port}/events?{paramName}={paramValue}";

    //TODO: should we move invitations to events?
    protected static final String CREATE_USER_URL  = "http://localhost:{port}/users";
    protected static final String GET_USER_INVITES_BY_ID = "http://localhost:{port}/users/{userId}/invites";
    protected static final String GET_USER_INVITES_BY_STATUS = "http://localhost:{port}/users/{userId}/invites?status={status}";
    protected static final String UPDATE_USER_INVITE = "http://localhost:{port}/users/{userId}/invites/{eventId}";


    @Autowired
    @Qualifier("testRestTemplate")
    protected TestRestTemplate debutClient;

    @LocalServerPort
    protected int port;

    @Autowired
    protected DebutWorld world;

}
