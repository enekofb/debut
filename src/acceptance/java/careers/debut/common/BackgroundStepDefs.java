package careers.debut.common;

import careers.debut.events.models.Event;
import careers.debut.events.models.Invite;
import careers.debut.events.models.InvitationStatus;
import careers.debut.users.models.User;
import careers.debut.events.repository.EventRepository;
import careers.debut.users.repository.UserRepository;
import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import org.springframework.beans.factory.annotation.Autowired;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

public class BackgroundStepDefs extends CommonDebutStepDefs {

    @Autowired
    protected EventRepository eventRepository;


    @Autowired
    protected UserRepository userRepository;

    @Given("^the following events exist in the system$")
    public void theFollowingEventsExistInTheSystem(DataTable eventsAsDatatable) throws Throwable {
        eventRepository.deleteAll();
        List<Map<String,String>> eventMapAsList = eventsAsDatatable
                .asMaps(String.class,String.class);
        List<Event> createdEventDtos = eventMapAsList.stream()
                .map(this::createEventFromMap)
                .collect(Collectors.toList());
        assertEquals(eventMapAsList.size(), createdEventDtos.size());
        world.setEvents(createdEventDtos);
    }

    Event createEventFromMap(Map<String,String> eventAsMap){

        LocalDate date = mapDate(eventAsMap.get("date"));

        Event event = Event.builder()
                .name(eventAsMap.get("eventName"))
                .location(eventAsMap.get("location"))
                .date(date)
                .build();
        return eventRepository.save(event);
    }

    private LocalDate mapDate(String date) {
        switch (date){
            case "today":
                return LocalDate.now();
            case "month":
                return LocalDate.now().plusDays(30);
            default:
                return LocalDate.parse(date);
        }
    }

    @Given("^the following users exist in the system$")
    public void theFollowingUsersExistInTheSystem(DataTable usersAsDatatable) throws Throwable {
        userRepository.deleteAll();
        List<Map<String,String>> userMapAsList = usersAsDatatable
                .asMaps(String.class,String.class);
        List<User> createUserDtos = userMapAsList.stream()
                .map(this::createUserFromMap)
                .collect(Collectors.toList());
        assertEquals(userMapAsList.size(), createUserDtos.size());
        world.setUsers(createUserDtos);
    }

    User createUserFromMap(Map<String,String> userAsMap){
        User user = User.builder()
                .email(userAsMap.get("userEmail"))
                .build();
        return userRepository.save(user);
    }

    @And("^the following invitations exists in the system$")
    public void theFollowingInvitationsExistsInTheSystem(DataTable invitationsAsDatatable) throws Throwable {
        createInvites(invitationsAsDatatable);
    }

    @Transactional
    public void createInvites(DataTable invitationsAsDatatable){

        List<Map<String,String>> invitationsAsList = invitationsAsDatatable
                .asMaps(String.class,String.class);

        invitationsAsList.stream()
                .map(this::createInvitationFromMap)
                .collect(Collectors.toList());
    }

    Invite createInvitationFromMap(Map<String, String> invitationAsMap) {

        InvitationStatus invitationStatus = InvitationStatus.valueOf(invitationAsMap.get("invitationStatus"));
        Event event = world.getEventIdByName(invitationAsMap.get("eventName"));
        User user = world.getUserByEmail(invitationAsMap.get("userEmail"));
        Invite invitation = new Invite(event.getId(),user.getId(),invitationStatus );
        event.addInvitee(invitation);
        user.addInvitee(invitation);
        eventRepository.save(event);
        userRepository.save(user);
        return invitation;
    }
}
