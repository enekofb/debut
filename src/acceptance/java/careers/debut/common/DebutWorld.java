package careers.debut.common;

import careers.debut.events.models.Event;
import careers.debut.users.models.User;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class DebutWorld {

    private Map<String,Event> eventsByName;

    private Map<String,User> usersByEmail;

    public void setEvents(Collection<Event> events){
        eventsByName = new HashMap<>();
        events.stream().forEach(event ->
                eventsByName.put(event.getName(),event));
    }

    public void setUsers(Collection<User> users){
        usersByEmail = new HashMap<>();
        users.stream().forEach(user ->
                usersByEmail.put(user.getEmail(),user));
    }


    public Event getEventIdByName(String name) {
        return this.eventsByName.get(name);

    }

    public User getUserByEmail(String email) {
        return this.usersByEmail.get(email);
    }

}
