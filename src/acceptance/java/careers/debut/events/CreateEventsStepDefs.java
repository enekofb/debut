package careers.debut.events;

import careers.debut.common.CommonDebutStepDefs;
import careers.debut.events.dto.CreateEventDto;
import careers.debut.events.dto.EventDto;
import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class CreateEventsStepDefs extends CommonDebutStepDefs {

    private EventDto createdEventDto;
    private CreateEventDto createEventDto;
    private CreateEventDto.CreateEventDtoBuilder eventBuilder;

    @Given("^I want to create an event with name \"([^\"]*)\"$")
    public void iWantToCreateAnEventWithName(String name) throws Throwable {
        eventBuilder = CreateEventDto.builder()
                .name(name);

    }

    @Given("^a list of invitees for the event$")
    public void a_list_of_invitees_for_the_event(DataTable inviteesDatatable) throws Throwable {
        eventBuilder = eventBuilder.
                invitees(inviteesDatatable.asList(String.class));
    }

    @And("^the event has max (\\d+) seats$")
    public void theEventHasMaxSeats(int numberOfSeats) throws Throwable {
        eventBuilder = eventBuilder.capacity(numberOfSeats);
    }

    @And("^the visibility is \"([^\"]*)\"$")
    public void theVisibilityIs(String visibility) throws Throwable {
       eventBuilder = eventBuilder.visibility(visibility);
    }

    @And("^the location is \"([^\"]*)\"$")
    public void theLocationIs(String location) throws Throwable {
        eventBuilder = eventBuilder.location(location);
    }

    @When("^I create the event$")
    public void i_create_the_event() throws Throwable {
        createEventDto = eventBuilder
                .build();
        assertThat(createEventDto, is(notNullValue()));
        createdEventDto = debutClient
                .postForObject(CREATE_EVENT_URL, createEventDto,
                        EventDto.class, port);
        assertThat(createdEventDto.getId(), is(notNullValue()));
    }

    @Then("^the event has been created$")
    public void the_event_has_been_created() throws Throwable {
        assertCreatedEvent(createEventDto,createdEventDto);
    }

    private void assertCreatedEvent(CreateEventDto createEventDto, EventDto createdEventDto) {
        assertThat(createEventDto.getName(),
                equalTo(createdEventDto.getName()));
        assertThat(createEventDto.getCapacity(),
                equalTo(createdEventDto.getCapacity()));
        assertThat(createEventDto.getVisibility(),
                equalTo(createdEventDto.getVisibility()));
        assertThat(createEventDto.getLocation(),
                equalTo(createdEventDto.getLocation()));
        assertThat(createEventDto.getInvitees().size(),
                equalTo(createdEventDto.getInvitees().size()));
    }
}
