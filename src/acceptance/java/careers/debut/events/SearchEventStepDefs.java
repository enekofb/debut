package careers.debut.events;

import careers.debut.common.CommonDebutStepDefs;
import careers.debut.events.dto.EventDto;
import cucumber.api.java.en.Then;

import java.util.Arrays;
import java.util.Collection;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNull.notNullValue;

public class SearchEventStepDefs extends CommonDebutStepDefs {


    Collection<EventDto> foundEvents;

    @cucumber.api.java.en.When("^I search al the events in \"([^\"]*)\"$")
    public void iSearchAlTheEventsIn(String location) throws Throwable {
        EventDto[] foundEventsDto = debutClient
                .getForObject(GET_EVENTS_BY_CRITERIA_URL,
                        EventDto[].class, port,"location",location);

        assertThat(foundEventsDto,is(notNullValue()));
        foundEvents = Arrays.asList(foundEventsDto);
        assertThat(foundEvents,is(notNullValue()));
    }

    @Then("^I receive a list of event with \"([^\"]*)\"$")
    public void iReceiveAListOfEventWith(String numberOfEvents) throws Throwable {
        assertThat(foundEvents.size(),equalTo(Integer.valueOf(numberOfEvents)));
    }
}
