package careers.debut.events;

import careers.debut.common.CommonDebutStepDefs;
import careers.debut.events.models.Invite;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;


public class GetEventInviteeStepDefs extends CommonDebutStepDefs{


    private Invite[] invitees;

    @When("^I get the invitees list for event \"([^\"]*)\"$")
    public void iGetTheInviteesListForEvent(String eventName) throws Throwable {

        String eventId = world.getEventIdByName(eventName).getId();
        invitees  = debutClient
                .getForObject(GET_EVENT_INVITEE_BY_ID,
                        Invite[].class, port,eventId);

    }

    @When("^I get list of accepted invitations for event \"([^\"]*)\"$")
    public void iGetListOfAcceptedInvitationsForEvent(String eventName) throws Throwable {
        String eventId = world.getEventIdByName(eventName).getId();
        invitees  = debutClient
                .getForObject(GET_EVENT_INVITEE_BY_STATUS,
                        Invite[].class, port,eventId,"ACCEPTED");
    }

    @When("^I get list of rejected invitations for event \"([^\"]*)\"$")
    public void iGetListOfRejectedInvitationsForEvent(String eventName) throws Throwable {
        String eventId = world.getEventIdByName(eventName).getId();
        invitees  = debutClient
                .getForObject(GET_EVENT_INVITEE_BY_STATUS,
                        Invite[].class, port,eventId,"REJECTED");
    }

    @Then("^I receive a invitee list of event with \"([^\"]*)\"$")
    public void iReceiveAInviteeListOfEventWith(String inviteeSize) throws Throwable {
        assertThat(invitees.length,equalTo(Integer.valueOf(inviteeSize)));
    }

}
