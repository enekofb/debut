package careers.debut.events;

import careers.debut.common.CommonDebutStepDefs;
import careers.debut.events.dto.EventDto;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import java.util.Arrays;
import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNull.notNullValue;

public class GetUpcomingEventsStepDefs extends CommonDebutStepDefs {


    Collection<EventDto> foundEvents;

    @When("^I search the \"([^\"]*)\" events to come$")
    public void iSearchTheEventsToCome(String maxNumberOfEvents) throws Throwable {
        EventDto[] foundEventsDto = debutClient
                .getForObject(GET_ALL_EVENTS_SORTED_BY,
                        EventDto[].class, port, "0", maxNumberOfEvents, "ASC", "date");

        assertThat(foundEventsDto, is(notNullValue()));
        foundEvents = Arrays.asList(foundEventsDto);
        assertThat(foundEvents, is(notNullValue()));

    }

    @Then("^I receive a list with following events \"([^\"]*)\"$")
    public void iReceiveAListOfEventWith(String eventsFoundName) throws Throwable {
        Set<String> foundEventNamesSet = foundEvents.stream().map(EventDto::getName).collect(Collectors.toSet());
        Set<String> expectedEventNamesSet = Arrays.stream(eventsFoundName.split(",")).collect(Collectors.toSet());
        assertThat(expectedEventNamesSet, equalTo(foundEventNamesSet));

    }
}
