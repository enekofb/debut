package careers.debut.events;

import careers.debut.common.CommonDebutStepDefs;
import careers.debut.events.dto.EventDtoWithForecast;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.core.IsNull.notNullValue;

public class GetEventForecastStepDefs extends CommonDebutStepDefs {
    private String eventId;

    ResponseEntity<EventDtoWithForecast>  eventDtoWithForecastResponse;

    @Given("^I am going to event \"([^\"]*)\"$")
    public void iAmGoingToEvent(String eventName) throws Throwable {
        eventId = world.getEventIdByName(eventName).getId();
        assertThat(eventId, is(notNullValue()));
    }

    @When("^I get its forecast$")
    public void iGetItsForecast() throws Throwable {
        eventDtoWithForecastResponse  = debutClient
                .getForEntity(GET_EVENT_FORECAST_BY_ID,
                        EventDtoWithForecast.class, port,eventId);
        assertThat(eventDtoWithForecastResponse, is(notNullValue()));
    }

    @Then("^I receive its forecast$")
    public void iReceiveItsForecast() throws Throwable {
        assertThat(eventDtoWithForecastResponse.getBody().getForecastDetails(),
                containsString("country"));
    }

    @Then("^I dont receive its forecast so its not predictable yet$")
    public void iDontReceiveItsForecastSoItsNotPredictableYet() throws Throwable {
        assertThat(eventDtoWithForecastResponse.getStatusCode(), equalTo(HttpStatus.EXPECTATION_FAILED));
    }
}
