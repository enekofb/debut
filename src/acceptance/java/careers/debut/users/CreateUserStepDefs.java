package careers.debut.users;

import careers.debut.common.CommonDebutStepDefs;
import careers.debut.users.dto.UserDto;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertEquals;

public class CreateUserStepDefs extends CommonDebutStepDefs{

    private UserDto createUserDto;
    private UserDto createdUserDto;

    @When("^I create a user with email \"([^\"]*)\"$")
    public void iCreateAUserWithEmail(String userEmail) throws Throwable {
        createUserDto = UserDto.builder()
                .email(userEmail)
                .build();
        createdUserDto = debutClient
                .postForObject(CREATE_USER_URL, createUserDto,
                        UserDto.class, port);
        assertThat(createdUserDto.getId(), is(notNullValue()));
    }

    @Then("^the user has been created$")
    public void theUserHasBeenCreated() throws Throwable {
        assertEquals(createUserDto.getEmail(),createdUserDto.getEmail());
    }
}
