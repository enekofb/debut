package careers.debut.users;

import careers.debut.common.CommonDebutStepDefs;
import careers.debut.events.models.InvitationStatus;
import careers.debut.events.models.Invite;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;


public class UpdateUserInviteStepDefs extends CommonDebutStepDefs{


    private String userId;
    private String eventId;
    private String statusUpdate;
    private Invite udpatedInvite;

    @Given("^I get the invite for event \"([^\"]*)\"  for user \"([^\"]*)\"$")
    public void iGetTheInviteForEventForUser(String eventName, String userEmail) throws Throwable {
        userId = world.getUserByEmail(userEmail).getId();
        assertThat(userId,is(notNullValue()));
        eventId = world.getEventIdByName(eventName).getId();
        assertThat(eventId,is(notNullValue()));
    }

    @When("^I update the invite status to \"([^\"]*)\"$")
    public void iUpdateTheInviteStatusTo(String statusUpdate) throws Throwable {
        this.statusUpdate = statusUpdate;

        Invite invite = new Invite(eventId,userId, InvitationStatus.valueOf(statusUpdate));

        HttpEntity<Invite> requestEntity = new HttpEntity<Invite>(invite);
        HttpEntity<Invite> response = debutClient.exchange(UPDATE_USER_INVITE,
                HttpMethod.PUT, requestEntity, Invite.class,port,userId,eventId);

        udpatedInvite = response.getBody();

        assertThat(udpatedInvite,is(notNullValue()));
    }

    @Then("^the invite has been updated$")
    public void theInviteHasBeenUpdated() throws Throwable {
        assertThat(udpatedInvite.getStatus(),equalTo(InvitationStatus.valueOf(statusUpdate)));
    }
}
