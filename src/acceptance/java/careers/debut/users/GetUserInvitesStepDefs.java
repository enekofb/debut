package careers.debut.users;

import careers.debut.common.CommonDebutStepDefs;
import careers.debut.events.models.Invite;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;


public class GetUserInvitesStepDefs extends CommonDebutStepDefs{


    private Invite[] invites;

    @When("^I get the invite list for user \"([^\"]*)\"$")
    public void iGetTheInvitationListForUser(String userEmail) throws Throwable {
        String userId = world.getUserByEmail(userEmail).getId();
        invites  = debutClient
                .getForObject(GET_USER_INVITES_BY_ID,
                        Invite[].class, port,userId);
    }

    @Then("^I receive an invite with \"([^\"]*)\"$")
    public void iReceiveAnInvitationWith(String numberOfInvites) throws Throwable {
        assertThat(invites.length,equalTo(Integer.valueOf(numberOfInvites)));
    }

    @When("^I get list of accepted invites for user \"([^\"]*)\"$")
    public void iGetListOfAcceptedInvitesForUser(String userEmail) throws Throwable {
        String userId = world.getUserByEmail(userEmail).getId();
        invites  = debutClient
                .getForObject(GET_USER_INVITES_BY_STATUS,
                        Invite[].class, port,userId,"ACCEPTED");
    }
}
