package careers.debut.events.service;

import careers.debut.events.dto.CreateEventDto;
import careers.debut.events.dto.EventDto;
import careers.debut.events.dto.EventDtoWithForecast;
import careers.debut.events.models.Event;
import careers.debut.events.models.Invite;
import careers.debut.events.models.InvitationStatus;
import careers.debut.users.models.User;
import careers.debut.events.repository.EventRepository;
import careers.debut.users.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import java.lang.reflect.Type;
import java.time.LocalDate;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class EventServiceTest {

    @Spy
    ModelMapper mapper;

    @Mock
    EventRepository eventRepository;

    @Mock
    UserService userService;

    @Mock
    ForecastService forecastService;

    @InjectMocks
    private EventService eventService;

    @Test
    public void mapCreateEventDtoToEventWithoutInvitee() throws Exception {
        CreateEventDto createEventDto = mock(CreateEventDto.class);
        Event event = mock(Event.class);

        doReturn(event).when(mapper).map(createEventDto, Event.class);
        Event mappedEvent = eventService.mapCreateEventDtoToEvent(createEventDto);
        verify(mapper).map(createEventDto, Event.class);

        assertThat(event, equalTo(mappedEvent));
    }


    @Test
    public void mapCreateEventDtoToEventWithInvitee() throws Exception {
        Event event = spy(Event.class);
        String userEmail = "john@gmail.com";
        Collection<String> userEmails = Collections.singleton(userEmail);

        CreateEventDto createEventDto = mock(CreateEventDto.class);
        when(createEventDto.getInvitees()).thenReturn(userEmails);

        User user = mock(User.class);
        Collection<User> users = Collections.singleton(user);

        doReturn(event).when(mapper).map(createEventDto, Event.class);
        when(userService.findAllByEmail(userEmails)).thenReturn(users);

        Event mappedEvent = eventService.mapCreateEventDtoToEvent(createEventDto);

        verify(mapper).map(createEventDto, Event.class);
        verify(userService).findAllByEmail(userEmails);

        assertThat(event, equalTo(mappedEvent));
        assertThat(mappedEvent.getInvitees().size(), equalTo(1));
    }


    @Test
    public void createEventWithoutInvitee() throws Exception {
        testCreateEvent(Collections.emptyList());
    }

    @Test
    public void createEventWithInvitee() throws Exception {
        testCreateEvent(Collections.singleton("john@google.com"));
    }

    private void testCreateEvent(Collection<String> userEmails) {
        String eventId = "event1234";
        Collection<User> users = userEmails.stream()
                .map(emai -> mock(User.class))
                .collect(Collectors.toList());

        CreateEventDto createEventDto = mock(CreateEventDto.class);
        when(createEventDto.getInvitees()).thenReturn(userEmails);
        when(createEventDto.getVisibility()).thenReturn("PUBLIC");

        EventDto eventDto = spy(EventDto.class);
        when(eventDto.getId()).thenReturn(eventId);

        Event event = spy(Event.class);
        when(event.getId()).thenReturn(eventId);
        when(event.getVisibility()).thenReturn(Event.Visibility.PUBLIC);

        doReturn(event).when(mapper).map(createEventDto, Event.class);

        when(eventRepository.save(event)).thenReturn(event);
        when(userService.findAllByEmail(userEmails)).thenReturn(users);

        EventDto createdEvent = eventService.createEvent(createEventDto);

        verify(mapper).map(createEventDto, Event.class);
        verify(eventRepository).save(event);

        assertThat(createdEvent.getId(), equalTo(event.getId()));
        assertThat(createdEvent.getInvitees().size(), equalTo(userEmails.size()));
    }


    @Test
    public void findByLocation() throws Exception {

        String location = "London";

        Type listType = new TypeToken<Collection<EventDto>>() {}.getType();

        Collection<Event> events = Collections.EMPTY_LIST;

        Collection<EventDto> eventsDto = Collections.EMPTY_LIST;

        when(eventRepository.findByLocation(location))
                .thenReturn(events);

        Collection<EventDto> foundEventsByLocation = eventService.
                findByLocation(location);

        verify(eventRepository).findByLocation(location);

        assertThat(foundEventsByLocation, equalTo(eventsDto));

    }


    @Test
    public void getInviteeForEventByEventId() throws Exception {
        String userId = "user123";
        User user = mock(User.class);
        when(user.getId()).thenReturn(userId);

        String eventId = "event123";
        Event event = mock(Event.class);
        when(event.getId()).thenReturn(eventId);

        Invite eventInvitee = new Invite( eventId, userId, InvitationStatus.ACCEPTED);
        when(event.getInvitees()).thenReturn(Collections.singleton(eventInvitee));

        when(eventRepository.findOne(eventId)).thenReturn(event);

        Collection<Invite> eventInvitees = eventService.getInviteeForEventByEventId(eventId);

        verify(eventRepository).findOne(eventId);

        assertThat(eventInvitees.size(), equalTo(1));
        assertThat(eventInvitees.iterator().next().getEvent(), equalTo(eventId));
    }

    @Test
    public void getInviteeForEventByEventIdAndStatus() throws Exception {

        String userId = "user123";
        User user = mock(User.class);
        when(user.getId()).thenReturn(userId);

        String eventId = "event123";
        Event event = mock(Event.class);
        when(event.getId()).thenReturn(eventId);

        Invite eventInvitee = new Invite(eventId, userId, InvitationStatus.ACCEPTED);
        when(event.getInvitees()).thenReturn(Collections.singleton(eventInvitee));

        when(eventRepository.findOne(eventId)).thenReturn(event);

        Collection<Invite> receivedEventInvitee = eventService.getInviteeForEventByEventIdAndStatus(eventId,InvitationStatus.ACCEPTED);

        verify(eventRepository).findOne(eventId);

        assertThat(receivedEventInvitee.size(), equalTo(1));
        assertThat(receivedEventInvitee.iterator().next().getEvent(), equalTo(eventId));


    }

    @Test
    public void getEventForecastForPredictableEvent() throws Exception {

        String eventId = "event123";
        String eventLocation = "London";
        String londonForecast = "always sunny";

        Event event = spy(Event.class);
        when(event.getId()).thenReturn(eventId);
        when(event.getDate()).thenReturn(LocalDate.now());
        when(event.getLocation()).thenReturn(eventLocation);

        when(eventRepository.findOne(eventId)).thenReturn(event);
        when(forecastService.getForecastForLocation(eventLocation)).thenReturn(londonForecast);

        EventDtoWithForecast eventForecast = eventService.getEventForecastById(eventId);

        verify(eventRepository).findOne(eventId);
        verify(forecastService).getForecastForLocation(eventLocation);

        assertThat(eventForecast.getId(),equalTo(eventId));
        assertThat(eventForecast.getForecastDetails(),equalTo(londonForecast));
    }


    @Test(expected = RuntimeException.class)
    public void getEventForecastForNonPredictableEvent() throws Exception {

        String eventId = "event123";

        Event event = spy(Event.class);
        when(event.getId()).thenReturn(eventId);
        when(event.getDate()).thenReturn(LocalDate.now().plusDays(10));

        when(eventRepository.findOne(eventId)).thenReturn(event);

        eventService.getEventForecastById(eventId);

        verify(eventRepository).findOne(eventId);

    }

    @Test
    public void findAllWithDefaultPage() throws Exception {

        PageRequest defaultPageSettings = new PageRequest(0,20, Sort.Direction.ASC,"id");
        Map<String, String> filters = Collections.EMPTY_MAP;
        Page<Event> page = spy(Page.class);

        Event event = Event.builder()
                .location("london")
                .date(LocalDate.now())
                .invitees(Collections.emptySet())
                .name("eventName")
                .id("event1234")
                .build();

        List<Event> events = Collections.singletonList(event);

        when(page.getContent()).thenReturn(events);
        when(eventRepository.findAll(defaultPageSettings)).thenReturn(page);

        Collection<EventDto> receivedEvents = eventService.findAllWithFilters(filters);

        verify(eventRepository).findAll(defaultPageSettings);

        assertThat(receivedEvents.size(),equalTo(1));
        assertThat(receivedEvents.iterator().next().getId(),equalTo("event1234"));
    }


}