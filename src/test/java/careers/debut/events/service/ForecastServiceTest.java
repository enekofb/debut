package careers.debut.events.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.web.client.RestTemplate;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ForecastServiceTest {


    @Mock
    RestTemplate restTemplate;

    ForecastService forecastService;

    @Before
    public void setup(){

        forecastService = new ForecastService("apiKey","http://endpoint",restTemplate);
    }

    @Test
    public void getForecastForLocation() throws Exception {

        String forecast = "sunny as ever";
        String location="London";
        String weatherUrl = "http://endpoint?q=London&APPID=apiKey";

        when(restTemplate.getForObject(weatherUrl,String.class))
                .thenReturn(forecast);

        String forecastFound = forecastService.getForecastForLocation(location);

        verify(restTemplate).getForObject(weatherUrl,String.class);

        assertThat(forecastFound,equalTo(forecast));

    }

}