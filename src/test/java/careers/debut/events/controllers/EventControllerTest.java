package careers.debut.events.controllers;

import careers.debut.events.dto.CreateEventDto;
import careers.debut.events.dto.EventDto;
import careers.debut.events.dto.EventDtoWithForecast;
import careers.debut.events.models.InvitationStatus;
import careers.debut.events.models.Invite;
import careers.debut.events.service.EventService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.*;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class EventControllerTest {

    @Mock
    private EventService eventService;

    @InjectMocks
    private EventController eventController;

    @Test
    public void createEvent(){
        String eventId = "eventId1";

        CreateEventDto createEventDto = mock(CreateEventDto.class);

        EventDto eventDto = mock(EventDto.class);
        when(eventDto.getId()).thenReturn(eventId);

        when(eventService.createEvent(createEventDto)).thenReturn(eventDto);

        EventDto createdEventDto = eventController.createEvent(createEventDto);

        verify(eventService).createEvent(createEventDto);

        assertThat(createdEventDto.getId(),equalTo(eventDto.getId()));
        assertThat(createdEventDto.getId(),equalTo(eventDto.getId()));
    }

    @Test
    public void findEventsWithFiltersWithLocation() throws Exception {

        String location = "London";

        Map<String, String> filtersWithLocation = new HashMap<String, String>() {{
            put(EventController.FILTER_BY_LOCATION,location);
        }};

        Collection<EventDto> events = mock(Collection.class);

        when(eventService.findByLocation(location)).thenReturn(events);

        Collection<EventDto> foundEvents = eventController.findEventsWithFilters(filtersWithLocation);

        verify(eventService).findByLocation(location);

        assertThat(foundEvents,equalTo(events));

    }

    @Test
    public void findEventsForLocation() throws Exception {

        String locationKey = "location";
        String location = "London";

        HashMap<String, String> filters = new HashMap<>();
        filters.put(locationKey,location);

        String eventId = "event1234";
        EventDto eventDto = mock(EventDto.class);
        when(eventDto.getId()).thenReturn(eventId);

        when(eventService.findByLocation(location)).thenReturn(Collections.singleton(eventDto));
        Collection<EventDto> eventsWithFilters = eventController.findEventsWithFilters(filters);
        verify(eventService).findByLocation(location);

        assertThat(eventsWithFilters.size(),equalTo(1));
        assertThat(eventsWithFilters.iterator().next().getId(),equalTo(eventId));

    }

    @Test
    public void findEventsForGenericFilter() throws Exception {

        Map<String, String> filters = mock(HashMap.class);

        String eventId = "event1234";
        EventDto eventDto = mock(EventDto.class);
        when(eventDto.getId()).thenReturn(eventId);

        when(eventService.findAllWithFilters(filters)).thenReturn(Collections.singleton(eventDto));
        Collection<EventDto> eventsWithFilters = eventController.findEventsWithFilters(filters);
        verify(eventService).findAllWithFilters(filters);

        assertThat(eventsWithFilters.size(),equalTo(1));
        assertThat(eventsWithFilters.iterator().next().getId(),equalTo(eventId));

    }

    @Test
    public void getEventInviteesByEventId() throws Exception {

        String eventId = "event1234";
        EventDto eventDto = mock(EventDto.class);
        when(eventDto.getId()).thenReturn(eventId);

        String invitationId = "invitationId1234";
        Invite invitation = new Invite(eventId,"user",InvitationStatus.REJECTED);

        HashMap<String, String> filters = new HashMap<>();

        when(eventService.getInviteeForEventByEventId(eventId))
                .thenReturn(Arrays.asList(invitation));

        Collection<Invite> inviteesFound = eventController.getEventInviteesByEventIdAndStatus(eventId, filters);

        verify(eventService).getInviteeForEventByEventId(eventId);
        assertThat(inviteesFound.size(),equalTo(1));
        assertThat(inviteesFound.iterator().next().getEvent(),equalTo(eventId));

    }

    @Test
    public void getEventInviteesByEventWithStatusAccepted() throws Exception {

        String eventId = "event1234";
        EventDto eventDto = mock(EventDto.class);
        when(eventDto.getId()).thenReturn(eventId);

        String filterKey = "status";
        String filterValue = "ACCEPTED";

        HashMap<String, String> filters = new HashMap<>();
        filters.put(filterKey,filterValue);


        String invitationId = "invitationId1234";
        Invite invitation = new Invite(eventId,"user"
                ,InvitationStatus.ACCEPTED);

        when(eventService.getInviteeForEventByEventIdAndStatus(eventId, InvitationStatus.ACCEPTED))
                .thenReturn(Arrays.asList(invitation));

        Collection<Invite> inviteesFound = eventController.getEventInviteesByEventIdAndStatus(eventId, filters);

        verify(eventService).getInviteeForEventByEventIdAndStatus(eventId, InvitationStatus.ACCEPTED);
        assertThat(inviteesFound.size(),equalTo(1));
        Invite invitationDto = inviteesFound.iterator().next();
        assertThat(invitationDto.getEvent(),equalTo(eventId));
        assertThat(invitationDto.getStatus().name(),equalTo(filterValue));

    }


    @Test
    public void getEventForecastById() throws Exception {
        String eventId = "event1234";
        EventDto eventDto = mock(EventDto.class);
        when(eventDto.getId()).thenReturn(eventId);
        EventDtoWithForecast eventDtoWithForecast= new EventDtoWithForecast(eventId,"forecast");

        when(eventService.getEventForecastById(eventId)).thenReturn(eventDtoWithForecast);

        EventDtoWithForecast receivedForecast = eventController.getEventForecastById(eventId);

        verify(eventService).getEventForecastById(eventId);

        assertThat(receivedForecast,equalTo(eventDtoWithForecast));

    }
}