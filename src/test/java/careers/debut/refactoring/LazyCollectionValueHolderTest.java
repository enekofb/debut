package careers.debut.refactoring;

import lombok.val;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collection;

import static java.util.Collections.emptyList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.*;

/**
 * @author sebastian
 * @date 20/06/2017
 */
@RunWith(MockitoJUnitRunner.class)
public class LazyCollectionValueHolderTest {

    private LazyCollectionValueHolder<Object> valueHolder;

    @Mock
    private Collection<Object> delegate;

    @Before
    public void setup(){
        valueHolder = spy(new LazyCollectionValueHolder<Object>() {
            protected Collection<Object> doGet () {
                return delegate;
            }
        });
    }

    @Test
    public void testSearchShouldOnlyBePerformedTheFirstTimeAndInvokeDelegateSizeMethod(){
        executeCommonChecks(Collection::size);
    }

    @Test
    public void testSearchShouldOnlyBePerformedTheFirstTimeAndInvokeDelegateIsEmptyMethod() {
        executeCommonChecks(Collection::isEmpty);
    }

    @Test
    public void testSearchShouldOnlyBePerformedTheFirstTimeAndInvokeDelegateContainsMethod() {
        val o = new Object();
        executeCommonChecks(collection -> collection.contains(o));
    }

    @Test
    public void testSearchShouldOnlyBePerformedTheFirstTimeAndInvokeDelegateIteratorMethod() {
        executeCommonChecks(Collection::iterator);
    }

    @Test
    public void testSearchShouldOnlyBePerformedTheFirstTimeAndInvokeDelegateToArrayMethod() {
        executeCommonChecks(Collection::toArray);
    }

    @Test
    public void testSearchShouldOnlyBePerformedTheFirstTimeAndInvokeDelegateToArrayTypedMethod() {
        Object[] a = new Object[1];
        executeCommonChecks(collection -> collection.toArray(a));
    }

    @Test
    public void testSearchShouldOnlyBePerformedTheFirstTimeAndInvokeDelegateAddMethod() {
        Object a = new Object();
        executeCommonChecks(collection -> collection.add(a));
    }

    @Test
    public void testSearchShouldOnlyBePerformedTheFirstTimeAndInvokeDelegateRemoveMethod() {
        Object a = new Object();
        executeCommonChecks(collection -> collection.remove(a));
    }

    @Test
    public void testSearchShouldOnlyBePerformedTheFirstTimeAndInvokeDelegateContainsAllMethod() {
        Collection<Object> c = emptyList();
        executeCommonChecks(collection -> collection.containsAll(c));
    }

    @Test
    public void testSearchShouldOnlyBePerformedTheFirstTimeAndInvokeDelegateAddAllMethod() {
        Collection<Object> c = emptyList();
        executeCommonChecks(collection -> collection.addAll(c));
    }

    @Test
    public void testSearchShouldOnlyBePerformedTheFirstTimeAndInvokeRemoveAllMethod() {
        Collection<Object> c = emptyList();
        executeCommonChecks(collection -> collection.removeAll(c));
    }

    @Test
    public void testSearchShouldOnlyBePerformedTheFirstTimeAndInvokeRetainAllMethod() {
        Collection<Object> c = emptyList();
        executeCommonChecks(collection -> collection.retainAll(c));
    }

    @Test
    public void testSearchShouldOnlyBePerformedTheFirstTimeAndInvokeClearMethod() {
        VoidMethodInvoker invokeCall = Collection::clear;
        executeCommonForVoidChecks(invokeCall);
    }

    private <T> void executeCommonChecks(MethodInvoker<T> methodInvoker){

        T value1 = methodInvoker.invoke(valueHolder);
        T value2 = methodInvoker.invoke(valueHolder);

        methodInvoker.invoke(verify(delegate, times(2)));
        verify(valueHolder, times(1)).doGet();

        T expectedValue = methodInvoker.invoke(delegate);

        assertThat(value1, equalTo(expectedValue));
        assertThat(value2, equalTo(expectedValue));

    }

    private void executeCommonForVoidChecks(VoidMethodInvoker methodInvoker){

        methodInvoker.invoke(valueHolder);
        methodInvoker.invoke(valueHolder);

        methodInvoker.invoke(verify(delegate, times(2)));
        verify(valueHolder, times(1)).doGet();

    }

    @FunctionalInterface
    private interface VoidMethodInvoker {
        void invoke(Collection<Object> collection);
    }

    @FunctionalInterface
    private interface MethodInvoker<T> {
        T invoke(Collection<Object> collection);
    }

}
