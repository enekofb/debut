package careers.debut.users.service;

import careers.debut.events.models.Event;
import careers.debut.events.models.InvitationStatus;
import careers.debut.events.models.Invite;
import careers.debut.users.models.User;
import careers.debut.users.repository.UserRepository;
import careers.debut.users.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collection;
import java.util.Collections;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

    @Mock
    UserRepository userRepository;

    @InjectMocks
    private UserService userService;


    public void createUser() throws Exception {

        String userId = "user1234";

        User user = mock(User.class);
        when(user.getId()).thenReturn(userId);

        when(userRepository.save(user)).thenReturn(user);

        User createdUser = userService.createUser(user);

        verify(userRepository).save(user);
        assertThat(createdUser.getId(), equalTo(user.getId()));
    }

    @Test
    public void findAllByEmail() throws Exception {

        User user = mock(User.class);
        String userEmail = "john@gmail.com";

        when(userRepository.findByEmail(userEmail)).thenReturn(user);

        Collection<User> foundUsersByEmail = userService.
                findAllByEmail(Collections.singleton(userEmail));

        verify(userRepository).findByEmail(userEmail);

        assertThat(foundUsersByEmail.size(), equalTo(1));
        assertThat(foundUsersByEmail.iterator().next(), equalTo(user));


    }

    @Test
    public void updateInviteFromRejectedToAccepted() throws Exception {

        String eventId = "event123";
        Event event = mock(Event.class);
        when(event.getId()).thenReturn(eventId);

        String userId = "user123";
        User user = mock(User.class);
        when(user.getId()).thenReturn(userId);

        Invite invitation = new Invite(eventId,userId,InvitationStatus.REJECTED);

        when(user.getEventInvitations()).thenReturn(Collections.singleton(invitation));

        when(userRepository.findOne(userId)).thenReturn(user);
        when(userRepository.save(user)).thenReturn(user);

        Invite toUpdateInvitation = new Invite(eventId,userId,InvitationStatus.ACCEPTED);

        Invite updatedInvite = userService.updateInvite(userId,eventId,toUpdateInvitation);

        verify(userRepository).findOne(userId);
        verify(userRepository).save(user);

        assertThat(updatedInvite.getStatus(), equalTo(toUpdateInvitation.getStatus()));
    }

    @Test
    public void getInviteForUserByUserId() throws Exception {
        String userId = "user123";
        User user = mock(User.class);
        when(user.getId()).thenReturn(userId);

        String eventId = "event123";
        Event event = mock(Event.class);
        when(event.getId()).thenReturn(eventId);

        Invite userInvitation = new Invite(eventId, userId, InvitationStatus.ACCEPTED);
        when(user.getEventInvitations()).thenReturn(Collections.singleton(userInvitation));

        when(userRepository.findOne(userId)).thenReturn(user);

        Collection<Invite> userInvites = userService.getInviteForUserByUserId(userId);

        verify(userRepository).findOne(userId);

        assertThat(userInvites.size(), equalTo(1));
        assertThat(userInvites.iterator().next().getUser(), equalTo(userId));
    }

    @Test
    public void getInviteForUserByUserIdAndStatus() throws Exception {

        String userId = "user123";
        User user = mock(User.class);
        when(user.getId()).thenReturn(userId);

        String eventId = "event123";
        Event event = mock(Event.class);
        when(event.getId()).thenReturn(eventId);

        Invite userInvitation = new Invite( event.getId(), user.getId(), InvitationStatus.ACCEPTED);
        when(user.getEventInvitations()).thenReturn(Collections.singleton(userInvitation));

        when(userRepository.findOne(userId)).thenReturn(user);

        Collection<Invite> userInvites = userService.getInviteForUserByUserIdAndStatus(userId,InvitationStatus.ACCEPTED);

        verify(userRepository).findOne(userId);

        assertThat(userInvites.size(), equalTo(1));
        assertThat(userInvites.iterator().next().getEvent(), equalTo(eventId));




    }
}