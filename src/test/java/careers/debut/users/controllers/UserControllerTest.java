package careers.debut.users.controllers;

import careers.debut.events.models.InvitationStatus;
import careers.debut.events.models.Invite;
import careers.debut.users.controllers.UserController;
import careers.debut.users.dto.CreateUserDto;
import careers.debut.users.dto.UserDto;
import careers.debut.users.models.User;
import careers.debut.users.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.modelmapper.ModelMapper;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class UserControllerTest {

    @Mock
    private UserService userService;

    @Mock
    private ModelMapper userMapper;

    @InjectMocks
    private UserController userController;

    @Test
    public void createUser(){
        String userId = "userId1234";
        String userEmail = "john@gmail.com";

        CreateUserDto createUserDto = mock(CreateUserDto.class);
        when(createUserDto.getEmail()).thenReturn(userEmail);

        UserDto userDto = mock(UserDto.class);
        when(userDto.getId()).thenReturn(userId);

        User user = mock(User.class);
        when(user.getId()).thenReturn(userId);

        when(userMapper.map(createUserDto,User.class)).thenReturn(user);
        when(userService.createUser(user)).thenReturn(user);
        when(userMapper.map(user,UserDto.class)).thenReturn(userDto);

        UserDto createdUserDto = userController.createUser(createUserDto);

        verify(userService).createUser(user);

        assertThat(createdUserDto.getId(),equalTo(user.getId()));
    }

    @Test
    public void updateInvite() throws Exception {
        String userId = "userId";
        String eventId = "eventId";

        Invite invitation = new Invite(userId,eventId
                , InvitationStatus.ACCEPTED);

        when(userService.updateInvite(userId,eventId,invitation))
                .thenReturn(invitation);

        Invite updatedInvite = userController.updateInvite(userId, eventId, invitation);

        verify(userService).updateInvite(userId,eventId,invitation);
        assertThat(updatedInvite,equalTo(invitation));
    }

    @Test
    public void getUserInvitesByUserId() throws Exception {
        String userId = "userId";

        Invite invitation = new Invite("event",userId
                ,InvitationStatus.ACCEPTED);

        when(userService.getInviteForUserByUserId(userId))
                .thenReturn(Collections.singletonList(invitation));

        Collection<Invite> userInvites = userController.getUserInvitesByUserIdAndStatus(userId,
                Collections.EMPTY_MAP);

        verify(userService).getInviteForUserByUserId(userId);

        assertThat(userInvites.contains(invitation),equalTo(true));

    }



    @Test
    public void getUserInvitesByUserIdAndStatus() throws Exception {
        String userId = "userId";

        String filterKey = "status";
        String filterValue = "ACCEPTED";
        HashMap<String, String> filters = new HashMap<>();
        filters.put(filterKey,filterValue);


        Invite invitation = new Invite("event",userId
                ,InvitationStatus.ACCEPTED);

        when(userService.getInviteForUserByUserIdAndStatus(userId,InvitationStatus.ACCEPTED))
        .thenReturn(Collections.singletonList(invitation));

        Collection<Invite> userInvites = userController.getUserInvitesByUserIdAndStatus(userId, filters);

        verify(userService).getInviteForUserByUserIdAndStatus(userId,InvitationStatus.ACCEPTED);

        assertThat(userInvites.contains(invitation),equalTo(true));

    }


}